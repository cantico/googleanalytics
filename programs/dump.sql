CREATE TABLE `ganalytics_conversion` (
	`id_function` char(255) NOT NULL default '',
	`conversion_id` int(11) NOT NULL default '0',
	`conversion_language` varchar(5) NOT NULL default 'en_US',
	`conversion_format` int(11) NOT NULL default '1',
	`conversion_color` varchar(6) NOT NULL default '',
	`conversion_label` varchar(255) NOT NULL default '',
	`conversion_value` varchar(255) default '0',
	PRIMARY KEY (`id_function`)
);