<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';





class gAnalytics_ConfigurationPage
{


	private function getForm()
	{
		$W = bab_Widgets();
		$form = $W->Form();
		$form->setName('configuration')->addClass('BabLoginMenuBackground')->addClass('ganalytics-form');
		$form->setHiddenValue('tg', bab_rp('tg'));
		$form->colon();
		
		$form->getLayout()->setVerticalSpacing(1,'em');

		$label = $W->Label(ganalytics_translate('Google Analytics Account ID'));
		$input = $W->LineEdit()->setAssociatedLabel($label)->setName('analytics_id');

		$form->addItem($W->VBoxItems($label, $input));
		
		$label = $W->Label(ganalytics_translate('Allowed domain names (comma separated)'));
		$input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('allowed_domains');
		
		$form->addItem($W->VBoxItems($label, $input));

		$form->addItem(
				$W->SubmitButton()
				->setLabel(ganalytics_translate('Save'))
		);

		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/googleanalytics/');

		$analytics_id = $registry->getValue('analytics_id', '');
		$allowed_domains = $registry->getValue('allowed_domains');
		
		if (null === $allowed_domains)
		{
			// premier parametrage
			$allowed_domains = $_SERVER['HTTP_HOST'];
		}

		$form->setValues(
			array(
				'configuration' => array(
					'analytics_id' => $analytics_id,
					'allowed_domains' => $allowed_domains
				)
			)
		);

		return $form;
	}




	public function display()
	{
		$W = bab_Widgets();
		$page = $W->BabPage();
		$page->addStyleSheet($GLOBALS['babInstallPath'].'styles/addons/googleanalytics/main.css');
		

		$page->addItem($this->getForm());
		$page->displayHtml();
	}


	public function save($configuration)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/googleanalytics/');

		$registry->setKeyValue('analytics_id', $configuration['analytics_id']);
		$registry->setKeyValue('allowed_domains', $configuration['allowed_domains']);
	}
}


if (!bab_isUserAdministrator())
{
	return;
}


$page = new gAnalytics_ConfigurationPage;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();

