<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


function googleanalytics_onPageRefreshed(bab_eventPageRefreshed $event)
{
	$addon = bab_getAddonInfosInstance('googleanalytics');

	if (!$addon->isAccessValid())
	{
		return;
	}
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/googleanalytics/');
	
	$I = $registry->getValueEx(array('analytics_id', 'allowed_domains'));
	
	$analytics_id 		= null;
	$allowed_domains 	= null;
	
	foreach($I as $arr)
	{
		switch($arr['key'])
		{
			case 'analytics_id':	$analytics_id = $arr['value'];		break;
			case 'allowed_domains': $allowed_domains = $arr['value'];	break;
		}
	}
	
	
	gAnalytics_logPage($allowed_domains, $analytics_id);
	
	// conversions
	
	$nodeId = bab_siteMap::getPosition();
	
	if (null === $nodeId)
	{
		return;
	}
	
	$baseNodeId = bab_siteMap::getSitemapRootNode(); // should be Custom if on the sitemap_editor sitemap
	
	
	// trouver la position dans la plan du site modifie sous le basenode
	

	$sitemap = bab_siteMap::getFromSite();
	if (!isset($sitemap))
	{
		return;
	}
	$customNodes = $sitemap->getNodesByIndex('target', $nodeId);

	foreach($customNodes as $customNode)
	{
		/*@var $customNode bab_Node */
	
		// get the first custom node under baseNode
		$testNode = $customNode->parentNode();
		/*@var $testNode bab_Node */
		do {
	
			if ($baseNodeId === $testNode->getId())
			{
				$nodeId = $customNode;
				break;
			}
	
		} while($testNode = $testNode->parentNode());
	}
	
	gAnalytics_setConversion($nodeId);
}



/**
 * Add conversion code for this nodeId
 */
function gAnalytics_setConversion($nodeId)
{
	global $babDB;
	
	$res = $babDB->db_query('SELECT * FROM ganalytics_conversion WHERE id_function='.$babDB->quote($nodeId));
	
	if (0 === $babDB->db_num_rows($res))
	{
		return;
	}
	
	$conversion = $babDB->db_fetch_assoc($res);
	extract($conversion);
	
	if (empty($conversion_id))
	{
		return;
	}
	
	
	global $babBody;
	/*@var $babBody babBody */
	
	
	// check if the conversion value has been overwriten manually
	
	$ns = @bab_functionality::get('SitemapEditorNodeSection/GoogleConversionTracking');
	/*@var $ns Func_SitemapEditorNodeSection_GoogleConversionTracking */
	
	if ($ns && null !== $ns->conversionValue())
	{
		$conversion_value = $ns->conversionValue();
	}
	
	
	if (null === $conversion_value)
	{
		$conversion_variable = '';
	} else {
		$conversion_variable = 'var google_conversion_value = '.$conversion_value.';';
	}
	
	
	$conversion_color = strtolower($conversion_color);
	
	$babBody->babecho("
		<script type=\"text/javascript\">
			/* <![CDATA[ */
			var google_conversion_id = $conversion_id;
			var google_conversion_language = '$conversion_language';
			var google_conversion_format = '$conversion_format';
			var google_conversion_color = '$conversion_color';
			var google_conversion_label = '".bab_toHtml($conversion_label, BAB_HTML_JS)."';
			$conversion_variable
			/* ]]> */
		</script>
		<script type=\"text/javascript\" src=\"http://www.googleadservices.com/pagead/conversion.js\"></script>
		<noscript>
			<div style=\"display:inline;\">
			<img height=\"1\" width=\"1\" border=\"0\" src=\"http://www.googleadservices.com/pagead/conversion/$conversion_id/?value=$conversion_value&amp;label=".urlencode($conversion_label)."&amp;guid=ON&amp;script=0\">
			</div>
		</noscript>
	");
}




/**
 * Add the google analytics code to the page
 * 
 */ 
function gAnalytics_logPage($allowed_domains, $analytics_id)
{
	if (!isset($allowed_domains) || empty($allowed_domains) || empty($analytics_id))
	{
		return;
	}
	
	$arr = preg_split('/\s*,\s*/', $allowed_domains);
	
	if (!in_array($_SERVER['HTTP_HOST'], $arr))
	{
		return;
	}
	
	global $babBody;
	/*@var $babBody babBody */
	
	$ga = @bab_functionality::get('GoogleAnalytics');
	/* @var $ga Func_GoogleAnalytics */
	
	if (false === $ga)
	{
		return; // not installed?
	}
	
	$addon = bab_getAddonInfosInstance('googleanalytics');
	
	$ga->_setAccount($analytics_id);
	$ga->_trackPageview();
	

	
	$babBody->addJavascriptFile($addon->getTemplatePath().'Tag_google_analytics.js', true);
	
	$babBody->babecho('
		<script type="text/javascript">
		'.$ga->getJavascript().' 
		</script>
	');
}