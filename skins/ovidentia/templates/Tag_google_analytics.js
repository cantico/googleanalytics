var tagAnalyticsCNIL = {}

tagAnalyticsCNIL.CookieConsent = function() {
	// Remplacez la valeur UA-XXXXXX-Y par l'identifiant analytics de votre site.
	var gaProperty = 'UA-XXXXXX-Y';
	var gaDimensions = [];
		
	for(var i=0; i<_gaq.length;i++) {
		if (_gaq[i][0] === '_setAccount') {
			gaProperty = _gaq[i][1];
		}
		
		if (_gaq[i][0] === 'dimensions') {
			gaDimensions = _gaq[i][1];
		}
	}
		
		
	// Desactive le tracking si le cookie d’Opt-out existe deja.
	var disableStr = 'ga-disable' + gaProperty;
	var firstCall = false;
	var domaineName = '';

	//Cette fonction retourne la date d’expiration du cookie de consentement 

	function getCookieExpireDate() { 
	 var cookieTimeout = 33696000000;// Le nombre de millisecondes que font 13 mois 
	 var date = new Date();
	 date.setTime(date.getTime()+cookieTimeout);
	 var expires = "; expires="+date.toGMTString();
	 return expires;
	}
	
	function getDomainName() {
		if (domaineName != '') {
			return domaineName;
		} else {
			var hostname = document.location.hostname;
			if (hostname.indexOf("www.") === 0)
				hostname = hostname.substring(4);
			return hostname;
		}
	}
	
	//Cette fonction definie le perimetre du consentement ou de l'opposition  (en fonction du domaine)
    //Par defaut nous considerons que le domaine est tout ce qu'il y'a apres  "www"	
	function getCookieDomainName() {
		var hostname = getDomainName();
		var domain = "domain=" + "."+hostname;
		return domain;
	}


	//Cette fonction verifie si on  a deja obtenu le consentement de la personne qui visite le site
	function checkFirstVisit() {
	   var consentCookie =  getCookie('hasConsent'); 
	   if ( !consentCookie ) return true;
	}

	//Affiche une  banniere d'information en bas de la page
	 function showBanner(){
		var bodytag = document.getElementsByTagName('body')[0];
		var div = document.createElement('div');
		div.setAttribute('id','cookie-banner');
		div.setAttribute('width','70%');
		// Le code HTML de la demande de consentement
		// Vous pouvez modifier le contenu ainsi que le style
		div.innerHTML =  '<div style="background-color:#000;opacity:0.75;text-align:center;padding:5px;font-size:12px;font-weight:bold;border-top:1px solid #eeeeee;position:fixed;top:0;z-index:2147483647;width:100%;height:60px;color:white" id="cookie-banner-message-main" align="center"><p style="color:white; height:100%;width:75%; margin:20px auto 0 auto">Ce site utilise Google Analytics.\
		En continuant \u00e0 naviguer, vous nous autorisez \u00e0 d\u00e9poser un cookie \u00e0 des fins de \
		mesure d\'audience. \
		<a href="javascript:tagAnalyticsCNIL.CookieConsent.showInform()" style="text-decoration:underline;color:white;"> En savoir plus ou s\'opposer</a>.<span id="cookie-banner-close" style="position:absolute;margin-left:20px;top:0px;cursor:pointer;"><a onclick="tagAnalyticsCNIL.CookieConsent.gaOptout();tagAnalyticsCNIL.CookieConsent.hideInform();" style="text-decoration:none;color:white;">X</a></span></p></div>';
		bodytag.insertBefore(div,bodytag.firstChild); // Ajoute la banniere juste au debut de la page 
		document.getElementsByTagName('body')[0].className+=' cookiebanner';	
		createInformAndAskDiv();
	 }
		  
		  
	// Fonction utile pour recuperer un cookie a partire de son nom
	function getCookie(NameOfCookie)  {
		if (document.cookie.length > 0) {        
			begin = document.cookie.indexOf(NameOfCookie+"=");
			if (begin != -1)  {
				begin += NameOfCookie.length+1;
				end = document.cookie.indexOf(";", begin);
				if (end == -1) end = document.cookie.length;
				return unescape(document.cookie.substring(begin, end)); 
			}
		 }
		return null;
	}


	//Effectue une demande de confirmation de DNT pour les utilisateurs d'IE
	function askDNTConfirmation() {
		var r = confirm("La signal DoNotTrack de votre navigateur est activ\u00e9, confirmez vous activer la fonction DoNotTrack?")
		return r;
	}

	//Verifie la valeur de navigator.DoNotTrack pour savoir si le signal est active et est a 1
	function notToTrack() {
		if ( (navigator.doNotTrack && (navigator.doNotTrack=='yes' || navigator.doNotTrack=='1')) || ( navigator.msDoNotTrack && navigator.msDoNotTrack == '1') ) {
			return true;
		}
		
		return false;
	}

	//Si le signal est a 0 on considere que le consentement a deja ete obtenu
	function isToTrack() {
		if ( navigator.doNotTrack && (navigator.doNotTrack === 'no' || navigator.doNotTrack === 0 )) {
			return true;
		}
		return false;
	}
	   
	// Fonction d'effacement des cookies   
	function delCookie(name )   {
		var path = ";path=" + "/";




		var expiration = "Thu, 01-Jan-1970 00:00:01 GMT";       
		document.cookie = name + "=" + path +" ; "+ getCookieDomainName() + ";expires=" + expiration;
	}
	  
	// Efface tous les types de cookies utilises par Google Analytics    
	function deleteAnalyticsCookies() {
		var cookieNames = ["__utma","__utmb","__utmc","__utmz","_ga","_gat"]
		for (var i=0; i<cookieNames.length; i++)
			delCookie(cookieNames[i])
	}

	//La fonction qui informe et demande le consentement. Il s'agit d'un div qui apparait au centre de la page
	function createInformAndAskDiv() {
		var bodytag = document.getElementsByTagName('body')[0];
		var div = document.createElement('div');
		div.setAttribute('id','inform-and-ask');
		div.style.width= window.innerWidth+"px" ;
		div.style.height= window.innerHeight+"px";
		div.style.display= "none";
		div.style.position= "fixed";
		div.style.zIndex= "100000";
		// Le code HTML de la demande de consentement
		// Vous pouvez modifier le contenu ainsi que le style
		div.innerHTML =  '<div style="width: 300px; background-color: white; repeat scroll 0% 0% white; border: 1px solid #cccccc; padding :10px 10px;text-align:center; position: fixed; top:30px; left:50%; margin-top:0px; margin-left:-150px; z-index:100000; opacity:1" id="inform-and-consent">\
		<div><span><b>Les cookies Google Analytics</b></span></div><br><div>Ce site utilise  des cookies de Google Analytics,\
		ces cookies nous aident \u00e0 identifier le contenu qui vous interesse le plus ainsi qu\'\u00e0 rep\u00e9rer certains \
		dysfonctionnements. Vos donn\u00e9es de navigations sur ce site sont envoy\u00e9es \u00e0 Google Inc</div><div style="padding :10px 10px;text-align:center;"><button style="margin-right:50px;text-decoration:underline;" \
		name="S\'opposer" onclick="tagAnalyticsCNIL.CookieConsent.gaOptout();tagAnalyticsCNIL.CookieConsent.hideInform();" id="optout-button" >S\'opposer</button><button style="text-decoration:underline;" name="cancel" onclick="tagAnalyticsCNIL.CookieConsent.hideInform()" >Accepter</button></div></div>';
		bodytag.insertBefore(div,bodytag.firstChild); // Ajoute la banniere juste au debut de la page 
	}

	  

	function isClickOnOptOut( evt) { // Si le noeud parent ou le noeud parent du parent est la banniere, on ignore le clic
		return(evt.target.parentNode.id == 'cookie-banner' || evt.target.parentNode.parentNode.id =='cookie-banner' || evt.target.id == 'optout-button')
	}

	function consent(evt) {
		if (!isClickOnOptOut(evt) ) { // On verifie qu'il ne s'agit pas d'un clic sur la banniere
			if ( !clickprocessed) {
				evt.preventDefault();
				document.cookie = 'hasConsent=true; '+ getCookieExpireDate() +' ; ' +  getCookieDomainName() + ' ; path=/'; 
				callGoogleAnalytics();
				clickprocessed = true;
				window.setTimeout(function() {evt.target.click();}, 1000)
			} 
		}
	}

	// Cette fonction en test permet de faire une call GA  afin de pouvoir compter le nombre de visite sans faire de suivi des utilisateurs (fonction en cours de test)
	// Cela cree un evenement page qui est consultable depuis le panneau evenement de GA
	// Potentiellement cette methode pourrait etre utilise pour comptabiliser les click sur l'opt-out
	function callGABeforeConsent() {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');
		// Ici on desactive les cookie
		__gaTracker('create', gaProperty, { 'storage': 'none', 'clientId': '0'});
		__gaTracker('send', 'event', 'page', 'load', {'nonInteraction': 1});
	}


	// Tag Google Analytics, cette version est avec le tag Universal Analytics
	function callGoogleAnalytics() {
		if (firstCall) return;
		else firstCall = true;
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', gaProperty, 'auto');  // Replace with your property ID.
		
		for (var name in gaDimensions) {
			if (gaDimensions.hasOwnProperty(name)) {
				ga('set', name, gaDimensions[name]);
			}
		}
		
		ga('send', 'pageview');
	}

	return {
		
			// La fonction d'opt-out   
		 gaOptout: function() {
			document.cookie = disableStr + '=true;'+ getCookieExpireDate() + ' ; ' +  getCookieDomainName() +' ; path=/';       
			document.cookie = 'hasConsent=false;'+ getCookieExpireDate() + ' ; ' +  getCookieDomainName() + ' ; path=/';
			var div = document.getElementById('cookie-banner');
			// on considere que le site a ete visite
			clickprocessed = true;
			// Ci dessous le code de la banniere affichee une fois que l'utilisateur s'est oppose au depot
			// Vous pouvez modifier le contenu et le style
			if ( div!= null ) div.innerHTML = '<div style="background-color:#fff;text-align:center;padding:5px;font-size:12px;border-bottom:1px solid #eeeeee;" id="cookie-message"> Vous vous \u00eates oppos\u00e9 \
			au d\u00e9p\u00f4t de cookies de mesures d\'audience dans votre navigateur </div>'
			window[disableStr] = true;
			deleteAnalyticsCookies();
		},

		
		 showInform: function() {
			var div = document.getElementById("inform-and-ask");
			div.style.display = "";
		},
		  
		  
		 hideInform: function() {
			var div = document.getElementById("inform-and-ask");
			div.style.display = "none";
			var div = document.getElementById("cookie-banner");
			div.style.display = "none";
		},
		
		
		start: function() {
			//Ce bout de code verifie que le consentement n'a pas deja ete obtenu avant d'afficher
			// la banniere
			var consentCookie =  getCookie('hasConsent');
			clickprocessed = false; 
			if (!consentCookie) {//L'utilisateur n'a pas encore de cookie, on affiche la banniere et si il clique sur un autre element que la banniere, on enregistre le consentement
				if ( notToTrack() ) { //L'utilisateur a active DoNotTrack. Do not ask for consent and just opt him out
					tagAnalyticsCNIL.CookieConsent.gaOptout()
					//alert("You've enabled DNT, we're respecting your choice")
				} else {
					if (isToTrack() ) { //DNT is set to 0, no need to ask for consent just set cookies
						consent();
					} else {
						if (window.addEventListener) { // See note https://github.com/CNILlab/Cookie-consent_Google-Analytics/commit/e323b3be2c4a4d05300e35cdc11102841abdcbc9
						  // Standard browsers
						  window.addEventListener("load", showBanner, false);
						  document.addEventListener("click", consent, false);
						} else {
						  window.attachEvent("onload", showBanner);
						  document.attachEvent("onclick", consent);
						}
						callGABeforeConsent()
					}
				}
			} else {
				if (document.cookie.indexOf('hasConsent=false') > -1) 
					window[disableStr] = true;
				else 
					callGoogleAnalytics();
			}
		}
	}

};

bab_initFunctions.push(function() {
	
	if (_gaq === undefined) {
		// _gaq variable need to be in source
		return;
	}
	
	var cc = tagAnalyticsCNIL.CookieConsent();
	cc.start();
});